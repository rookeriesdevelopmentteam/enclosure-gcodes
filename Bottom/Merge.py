import glob
import shutil

out_filename = "bottom.nc"

with open(out_filename, 'wb') as outfile:
    for filename in glob.glob('*.nc'):
        if filename == out_filename:
            # don't want to copy the output into the output
            continue

        with open(filename, 'r+') as f:
            content = f.read()
            f.seek(0, 0)
            header = "(File = {})".format(filename)
            f.write('\n' + '\n' + header.rstrip('\r\n') + '\n' + content)

        with open(filename, 'rb') as readfile:
            shutil.copyfileobj(readfile, outfile)
